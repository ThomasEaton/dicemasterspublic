# Dice Masters #

This is an iOS app that was designed to manage and store your Marvel Dice Masters collection.  It relied on the Parse Backend API.  This project has received a few updates since Parse was shut down.  Considering a revival on the new Open Source Parse Platform (http://parseplatform.org/) or switching to another service.