//
//  DMTSetSelectionTableViewController.h
//  DiceMastersTracker
//
//  Created by Thomas Eaton on 2015-03-28.
//
//

#import <UIKit/UIKit.h>

@interface DMTSetSelectionTableViewController : UITableViewController

@property (nonatomic, strong) NSNumber *cellHeight;
@property (nonatomic, strong) NSDictionary *argumentArray;

@end
