//
//  DMTSetSelectionTableViewController.m
//  DiceMastersTracker
//
//  Created by Thomas Eaton on 2015-03-28.
//
//

#import "DMTSetSelectionTableViewController.h"
#import "DMTCollectionViewController.h"

#import <Parse/Parse.h>

@interface DMTSetSelectionTableViewController ()

@property (nonatomic, strong) NSArray *setArray;

@end

@implementation DMTSetSelectionTableViewController

NSString * const SET_TABLE_CELL = @"setTableCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.clearsSelectionOnViewWillAppear = YES;
    
    PFQuery *query = [PFQuery queryWithClassName:@"DiceMastersSet"];
    [query fromLocalDatastore];
    
    _setArray = [query findObjects];
    
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Only the set listing.
    // TODO - Change this when more games are added.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _setArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:SET_TABLE_CELL forIndexPath:indexPath];
    
    cell.tag = indexPath.item + 1;
    
    NSLog(@"Row: %ld", indexPath.item+1);
    
    UIImageView *imageView = ((UIImageView *)[cell viewWithTag:100]);
    imageView.image = [
                       UIImage imageNamed:
                       [NSString stringWithFormat:@"%ld-set", [_setArray[indexPath.item][@"setNumber"] longValue]]
                       ];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [_cellHeight intValue];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    UIViewController *collectionViewController = segue.destinationViewController;
    
    [collectionViewController setValue:[NSNumber numberWithLong:((UIView *)sender).tag] forKey:@"setNumber"];
    
    // Populate the Arguments
    if (_argumentArray) {
        for (NSString *argument in [_argumentArray allKeys]) {
            [collectionViewController setValue:[_argumentArray objectForKey:argument] forKey:argument];
        }
    }
}

@end
