//
//  DMTCollectionSelectTableViewController.m
//  DiceMastersTracker
//
//  Created by Thomas Eaton on 2015-03-30.
//
//

#import "DMTCollectionSelectTableViewController.h"

#import <Parse/Parse.h>

@interface DMTCollectionSelectTableViewController ()

@end

@implementation DMTCollectionSelectTableViewController

NSString *const MY_COLLECTION_CELL = @"myCollectionCell";
NSString *const LOG_IN_CELL = @"logInCell";
NSString *const FRIEND_COLLECTION_CELL = @"friendCollectionCell";

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // My collection or friend's collection.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // My collection or friend's collection.
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    
    
    if (indexPath.section == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:MY_COLLECTION_CELL forIndexPath:indexPath];
    } else {
        
        // You must be logged in to view a friend's collection.
        if ([PFAnonymousUtils isLinkedWithUser:[PFUser currentUser]]) {
            cell = [tableView dequeueReusableCellWithIdentifier:LOG_IN_CELL forIndexPath:indexPath];
        } else {
            cell = [tableView dequeueReusableCellWithIdentifier:FRIEND_COLLECTION_CELL forIndexPath:indexPath];
        }
    }
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return @"My Collection";
    } else {
        return @"Friend's Collections";
    }
}

@end
