//
//  DMTLogInBehaviour.h
//  DiceMastersTracker
//
//  Created by Thomas Eaton on 2015-03-30.
//
//

#import <Foundation/Foundation.h>

#import <ParseUI/ParseUI.h>

@interface DMTLogInBehaviour : NSObject <PFLogInViewControllerDelegate, PFSignUpViewControllerDelegate>

@property (nonatomic, weak) IBOutlet UIViewController *parentViewController;

- (IBAction)logInButtonPressed:(id)sender;

@end
