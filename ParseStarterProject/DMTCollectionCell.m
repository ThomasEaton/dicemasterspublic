//
//  DMTCollectionCell.m
//  DiceMastersTracker
//
//  Created by Thomas Eaton on 2015-03-29.
//
//

#import "DMTCollectionCell.h"

#import "DMTCollectionViewController.h"

@implementation DMTCollectionCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setUpCellwithCard:(PFObject *)cardObject withCardChangeCollection:(NSMutableArray *)cardAmounts
    withDiceChangeCollection:(NSMutableArray *)diceAmounts{
    
    
    self.cardObject = cardObject;
    self.cardChanges = cardAmounts;
    self.diceChanges = diceAmounts;
    
    self.cardImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"%d-%d.jpg", [cardObject[@"setNumber"] intValue], [cardObject[@"collectorNumber"] intValue]]];
    self.cardTitle.text = cardObject[@"cardName"];
    self.cardSubtitle.text = cardObject[@"subtitle"];
    
    // Find the Card Amounts from the Database.
    PFObject *currentCard = nil;
    for (PFObject *cardChange in cardAmounts) {
        if ([cardChange[@"card"] isEqual:cardObject]) {
            currentCard = cardChange;
        }
    }
    
    // If this is a card that you own, populate the quantities on the UI.
    if (cardAmounts != nil && currentCard != nil && [currentCard[@"amount"] intValue] != 0) {
        
        [_cardField setValue:[NSString stringWithFormat:@"%@", currentCard[@"amount"]] forKey:@"text"];
        [_cardStepper setValue:[NSNumber numberWithDouble:((double)[currentCard[@"amount"] intValue])] forKey:@"value"];
        
    } else {
        
        [_cardField setValue:[NSString stringWithFormat:@""] forKey:@"text"];
        [_cardStepper setValue:[NSNumber numberWithDouble:0.0] forKey:@"value"];
    }
    
    
    // Find the amount of Dice you own, independent of the cards.
    PFObject *currentDice = nil;
    for (PFObject *diceChange in _diceChanges) {
        if ([diceChange[@"cardName"] isEqual:_cardObject[@"cardName"]]) {
            currentDice = diceChange;
        }
    }
    
    // If you have dice amounts, populate the quantities.
    if (diceAmounts != nil && currentDice != nil && [currentDice[@"amount"] intValue] != 0) {
        
        [_diceField setValue:[NSString stringWithFormat:@"%@", currentDice[@"amount"]] forKey:@"text"];
        [_diceStepper setValue:[NSNumber numberWithDouble:((double)[currentDice[@"amount"] intValue])] forKey:@"value"];
        
    } else {
        [_diceField setValue:[NSString stringWithFormat:@""] forKey:@"text"];
        [_diceStepper setValue:[NSNumber numberWithDouble:0.0] forKey:@"value"];
    }
    [self updateDiceFieldColour];
    
    [self addObserver:self.diceField forKeyPath:@"text" options:NSKeyValueObservingOptionNew context:NULL];
}

- (void)cardStepperPressed:(id)sender {
    int amount = (int)_cardStepper.value;
    
    if (amount != 0) {
        [_cardField setValue:[NSString stringWithFormat:@"%d", amount] forKey:@"text"];
    } else {
        [_cardField setValue:@"" forKey:@"text"];
    }
    
    // Find and update the object if able.
    PFObject *foundObject = nil;
    for (PFObject *cardChange in _cardChanges) {
        if ([cardChange[@"card"] isEqual:_cardObject]) {
            foundObject = cardChange;
        }
    }
        
    if (foundObject == nil) {
        
        PFObject *playerCard = [PFObject objectWithClassName:@"PlayerCard"];
        playerCard[@"user"] = [PFUser currentUser];
        playerCard[@"card"] = _cardObject;
        playerCard[@"amount"] = [NSNumber numberWithInt:amount];
        
        [_cardChanges addObject:playerCard];
        
    } else {
        
        PFObject *playerCard = foundObject;
        playerCard[@"amount"] = [NSNumber numberWithInt:amount];
    }
}

- (void)diceStepperPressed:(id)sender {
    int amount = (int)_diceStepper.value;
    if (amount != 0) {
        [_diceField setValue:[NSString stringWithFormat:@"%d", amount] forKey:@"text"];
    } else {
        [_diceField setValue:@"" forKey:@"text"];
    }
    
    // Find and update the object if able.
    PFObject *foundObject = nil;
    for (PFObject *diceChange in _diceChanges) {
        if ([diceChange[@"cardName"] isEqual:_cardObject[@"cardName"]]) {
            foundObject = diceChange;
        }
    }
    
    if (foundObject == nil) {
        
        PFObject *playerDice = [PFObject objectWithClassName:@"PlayerDice"];
        playerDice[@"user"] = [PFUser currentUser];
        playerDice[@"setNumber"] = _cardObject[@"setNumber"];
        playerDice[@"cardName"] = _cardObject[@"cardName"];
        playerDice[@"amount"] = [NSNumber numberWithInt:amount];
        
        [_diceChanges addObject:playerDice];
        
    } else {
        
        PFObject *playerDice = foundObject;
        playerDice[@"amount"] = [NSNumber numberWithInt:amount];
    }
    [self updateDiceFieldColour];
    
    // Send a message to save the changes to the database.
    [[NSNotificationCenter defaultCenter] postNotificationName:DMTCollectionDiceUpdatedMessage object:nil userInfo:
     @{@"diceName" : _cardObject[@"cardName"], @"amount" : [NSNumber numberWithInt:amount] }];
}

- (void)diceUpdated:(NSString *)diceName withAmount:(NSNumber *)amount {
    
    if ([_cardObject[@"cardName"] isEqualToString:diceName]) {
        
        if ([amount intValue] != 0) {
            [_diceField setValue:[NSString stringWithFormat:@"%d", [amount intValue]] forKey:@"text"];
        } else {
            [_diceField setValue:@"" forKey:@"text"];
        }
        [self updateDiceFieldColour];
    }
}

- (void)updateDiceFieldColour {
    
    int diceAmount = [_diceField.text intValue];
        
    // Set the dice color to show the use the max number of dice.
    if (diceAmount == [_cardObject[@"dieLimit"] longValue]) {
        _diceField.textColor = [UIColor blackColor];
    } else if (diceAmount < [_cardObject[@"dieLimit"] longValue]) {
        _diceField.textColor = [UIColor redColor];
    } else {
        _diceField.textColor = [UIColor greenColor];
    }
}

@end
