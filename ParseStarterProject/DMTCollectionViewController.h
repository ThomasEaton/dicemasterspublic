//
//  DMTCollectionViewController.h
//  DiceMastersTracker
//
//  Created by Thomas Eaton on 2015-03-28.
//
//

#import <UIKit/UIKit.h>

@interface DMTCollectionViewController : UITableViewController
    <UISearchBarDelegate, UISearchDisplayDelegate, UISearchControllerDelegate>

extern NSString *const DMTCollectionDiceUpdatedMessage;

@property (nonatomic, strong) NSNumber *setNumber;
@property (nonatomic, strong) NSNumber *cellHeight;

@property (nonatomic) bool doNotQuery;

@end
