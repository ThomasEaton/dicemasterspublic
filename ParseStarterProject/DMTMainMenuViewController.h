//
//  DMTMainMenuViewController.h
//  DiceMastersTracker
//
//  Created by Thomas Eaton on 2015-03-30.
//
//

#import <UIKit/UIKit.h>

@interface DMTMainMenuViewController : UITableViewController

@property (nonatomic, weak) IBOutlet UIBarButtonItem *logInButton;

@end
