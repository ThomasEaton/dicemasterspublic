//
//  DMTLogInBehaviour.m
//  DiceMastersTracker
//
//  Created by Thomas Eaton on 2015-03-30.
//
//

#import "DMTLogInBehaviour.h"

#import <Parse/Parse.h>

@implementation DMTLogInBehaviour

- (void)logInButtonPressed:(id)sender {
    
    if ([PFAnonymousUtils isLinkedWithUser:[PFUser currentUser]]) {
        
        PFLogInViewController *logInViewController = [[PFLogInViewController alloc] init];
        logInViewController.fields = logInViewController.fields = (PFLogInFieldsUsernameAndPassword
                                                                   | PFLogInFieldsLogInButton
                                                                   | PFLogInFieldsSignUpButton
                                                                   | PFLogInFieldsPasswordForgotten
                                                                   | PFLogInFieldsDismissButton);
        
        logInViewController.delegate = self;
        
        PFSignUpViewController *signUpViewController = logInViewController.signUpController;
        signUpViewController.fields = (PFSignUpFieldsUsernameAndPassword
                                       | PFSignUpFieldsDismissButton);
        signUpViewController.delegate = self;
        
        [_parentViewController presentViewController:logInViewController animated:YES completion:nil];
    } else {
        
        [PFUser logOutInBackgroundWithBlock:^(NSError *error) {
            [PFAnonymousUtils logInWithBlock:^(PFUser *user, NSError *error) {
                if (error) {
                    NSLog(@"Anonymous login failed.");
                } else {
                    NSLog(@"Anonymous user logged in.");
                }
            }];
            
            [[PFUser currentUser] incrementKey:@"test"];
            [[PFUser currentUser] save];
        }];
    }
}

- (void)logInViewController:(PFLogInViewController *)controller
               didLogInUser:(PFUser *)user {
    [_parentViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)logInViewControllerDidCancelLogIn:(PFLogInViewController *)logInController {
    [_parentViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)signUpViewController:(PFSignUpViewController *)signUpController didSignUpUser:(PFUser *)user {
    [_parentViewController dismissViewControllerAnimated:YES completion:nil];
}

- (void)signUpViewControllerDidCancelSignUp:(PFSignUpViewController *)signUpController {
    [_parentViewController dismissViewControllerAnimated:YES completion:nil];
}

@end
