//
//  DMTCollectionViewController.m
//  DiceMastersTracker
//
//  Created by Thomas Eaton on 2015-03-28.
//
//

#import "DMTCollectionViewController.h"
#import "DMTCollectionCell.h"

#import <Parse/Parse.h>

@interface DMTCollectionViewController ()

@property (nonatomic, strong) NSArray *cards;
@property (nonatomic, strong) NSArray *basicActionCards;

@property (nonatomic, strong) NSArray *searchedCards;
@property (nonatomic, strong) NSArray *searchedBasicActionCards;

@property (atomic, strong) NSMutableArray *cardAmounts;
@property (atomic, strong) NSMutableArray *diceAmounts;

@end

@implementation DMTCollectionViewController

NSString *const DMTCollectionDiceUpdatedMessage = @"DMTCollectionDiceUpdatedMessage";

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.clearsSelectionOnViewWillAppear = YES;

    // Find all of the player's current cards.
    PFQuery *cardQuery = [PFQuery queryWithClassName:@"Card"];
    [cardQuery fromLocalDatastore];
    [cardQuery whereKey:@"setNumber" equalTo:_setNumber];
    [cardQuery whereKey:@"cardName" notEqualTo:@"Basic Action Card"];
    [cardQuery orderByAscending:@"collectorNumber"];
    
    _cards = [cardQuery findObjects];
    
    // Find all of the Basic Action Cards
    PFQuery *basicCardQuery = [PFQuery queryWithClassName:@"Card"];
    [basicCardQuery fromLocalDatastore];
    [basicCardQuery whereKey:@"setNumber" equalTo:_setNumber];
    [basicCardQuery whereKey:@"cardName" equalTo:@"Basic Action Card"];
    [basicCardQuery orderByAscending:@"collectorNumber"];
    
    _basicActionCards = [basicCardQuery findObjects];
    
    _searchedCards = [NSMutableArray arrayWithArray:_cards];
    _searchedBasicActionCards = [NSMutableArray arrayWithArray:_basicActionCards];
    
    if (!_doNotQuery) {
        
        // Find all of the players current cards and dice.
        PFQuery *setQuery = [PFQuery queryWithClassName:@"Card"];
        [setQuery fromLocalDatastore];
        [setQuery whereKey:@"setNumber" equalTo:_setNumber];
        setQuery.limit = 500;
        
        PFQuery *cardAmountQuery = [PFQuery queryWithClassName:@"PlayerCard"];
        [cardAmountQuery whereKey:@"user" equalTo:[PFUser currentUser]];
        [cardAmountQuery whereKey:@"card" matchesQuery:setQuery];
        cardAmountQuery.limit = 200;
        
        _cardAmounts = [[NSMutableArray alloc] initWithArray:[cardAmountQuery findObjects]];
        
        PFQuery *diceAmountQuery = [PFQuery queryWithClassName:@"PlayerDice"];
        [diceAmountQuery whereKey:@"user" equalTo:[PFUser currentUser]];
        [diceAmountQuery whereKey:@"setNumber" equalTo:_setNumber];
        diceAmountQuery.limit = 200;
        
        _diceAmounts = [[NSMutableArray alloc] initWithArray:[diceAmountQuery findObjects]];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] addObserverForName:DMTCollectionDiceUpdatedMessage object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *note) {
        
        NSArray *tableCells = [self.tableView visibleCells];
        
        for (DMTCollectionCell *cell in tableCells) {
            [cell diceUpdated:note.userInfo[@"diceName"] withAmount:note.userInfo[@"amount"]];
        }
    }];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [PFObject saveAllInBackground:_cardAmounts block:^(BOOL succeeded, NSError *error) {
        if (error) {
            NSLog(@"%@", error.description);
        } else {
            NSLog(@"Cards Updated: %lu", _cardAmounts.count);
        }
    }];
    
    [PFObject saveAllInBackground:_diceAmounts block:^(BOOL succeeded, NSError *error) {
        if (error) {
            NSLog(@"%@", error.description);
        } else {
            NSLog(@"Dice Updated: %lu", _diceAmounts.count);
        }
    }];
    
    [PFObject pinAllInBackground:_cardAmounts block:^(BOOL succeeded, NSError *error) {
        NSLog(@"Pinned Cards: %lu", _cardAmounts.count);
    }];
    
    [PFObject pinAllInBackground:_diceAmounts block:^(BOOL succeeded, NSError *error) {
        NSLog(@"Pinned Dice: %lu", _diceAmounts.count);
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 2;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0)
        return @"Cards";
    else
        return @"Basic Action Cards";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        if (section == 0)
            return _searchedCards.count;
        else
            return _searchedBasicActionCards.count;
    } else {
        if (section == 0)
            return _cards.count;
        else
            return _basicActionCards.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DMTCollectionCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"cardTableCell" forIndexPath:indexPath];
    cell.tag = [NSString stringWithFormat:@"%ld", indexPath.item];
    
    PFObject *cardObject;
    if (indexPath.section == 0) {
        
        if (tableView == self.searchDisplayController.searchResultsTableView) {
            cardObject = _searchedCards[indexPath.item];
        } else {
            cardObject = _cards[indexPath.item];
        }
        
    } else {
        
        if (tableView == self.searchDisplayController.searchResultsTableView) {
            cardObject = _searchedBasicActionCards[indexPath.item];
        } else {
            cardObject = _basicActionCards[indexPath.item];
        }
    }
    
    [cell setUpCellwithCard:cardObject withCardChangeCollection:_cardAmounts withDiceChangeCollection:_diceAmounts];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [_cellHeight intValue];
}

#pragma mark - Search Controller

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    NSCompoundPredicate *resultPredicate = [NSCompoundPredicate orPredicateWithSubpredicates:@[
        [NSPredicate predicateWithFormat:@"cardName contains[c] %@", searchText],
        [NSPredicate predicateWithFormat:@"subtitle contains[c] %@", searchText],
        [NSPredicate predicateWithFormat:@"rarity contains[c] %@", searchText],
        [NSPredicate predicateWithFormat:@"ability contains[c] %@", searchText],
        [NSPredicate predicateWithFormat:@"affiliation contains[c] %@", searchText],
        [NSPredicate predicateWithFormat:@"burstAbility contains[c] %@", searchText]
    ]];
    
    _searchedCards = [_cards filteredArrayUsingPredicate:resultPredicate];
    _searchedBasicActionCards = [_basicActionCards filteredArrayUsingPredicate:resultPredicate];
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}

@end
