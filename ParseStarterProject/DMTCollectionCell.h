//
//  DMTCollectionCell.h
//  DiceMastersTracker
//
//  Created by Thomas Eaton on 2015-03-29.
//
//

#import <UIKit/UIKit.h>
#import <JVFloatLabeledTextField.h>

#import <Parse/Parse.h>

@interface DMTCollectionCell : UITableViewCell <UITextFieldDelegate>

@property (nonatomic, strong) NSMutableArray *cardChanges;
@property (nonatomic, strong) NSMutableArray *diceChanges;

@property (nonatomic, strong) IBOutlet PFObject *cardObject;

@property (nonatomic, weak) IBOutlet UIView *view;
@property (nonatomic, weak) IBOutlet UIImageView *cardImage;
@property (nonatomic, weak) IBOutlet UILabel *cardTitle;
@property (nonatomic, weak) IBOutlet UILabel *cardSubtitle;
@property (nonatomic, weak) IBOutlet UITextField *cardField;
@property (nonatomic, weak) IBOutlet UIStepper *cardStepper;
@property (nonatomic, weak) IBOutlet UITextField *diceField;
@property (nonatomic, weak) IBOutlet UIStepper *diceStepper;

- (void)setUpCellwithCard:(PFObject *)cardObject
 withCardChangeCollection:(NSMutableArray *)cardAmounts
 withDiceChangeCollection:(NSMutableArray *)diceAmounts;

- (IBAction)cardStepperPressed:(id)sender;
- (IBAction)diceStepperPressed:(id)sender;

- (void)diceUpdated:(NSString *)diceName withAmount:(NSNumber *)amount;

@end
