//
//  ParseStarterProjectViewController.m
//
//  Copyright 2011-present Parse Inc. All rights reserved.
//

#import "DMTLaunchScreenViewController.h"

#import <Parse/Parse.h>

@interface DMTLaunchScreenViewController ()

@property (atomic) unsigned long cardsLoaded;
@property (nonatomic) unsigned long cardsToLoad;

@end


@implementation DMTLaunchScreenViewController

#pragma mark UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addObserver:self forKeyPath:@"cardsLoaded" options:NSKeyValueObservingOptionNew context:NULL];
    [self addObserver:self forKeyPath:@"cardsToLoad" options:NSKeyValueObservingOptionNew context:NULL];
    
    // Get a query of all of the sets.
    PFQuery *setQuery = [PFQuery queryWithClassName:@"DiceMastersSet"];
    [setQuery whereKey:@"active" equalTo:[NSNumber numberWithBool:YES]];
    [setQuery clearCachedResult];
    [setQuery findObjectsInBackgroundWithBlock:^(NSArray *activeSets, NSError *error) {
        
        // Unpin all sets that are unactive.
        NSMutableArray *activeSetsNumbers = [[NSMutableArray alloc] init];
        for (PFObject *activeSet in activeSets) {
            [activeSetsNumbers addObject:activeSet[@"setNumber"]];
        }
        
        PFQuery *inactiveSetsQuery = [PFQuery queryWithClassName:@"DiceMastersSet"];
        [inactiveSetsQuery fromLocalDatastore];
        [inactiveSetsQuery whereKey:@"setNumber" notContainedIn:activeSetsNumbers];
        NSArray *inactiveLocalSets = [inactiveSetsQuery findObjects];
        [PFObject unpinAll:inactiveLocalSets];
        
        // Get all of the sets to download.
        NSMutableDictionary *setVersions = [[NSMutableDictionary alloc] init];
        NSMutableArray *setsToDownload = [[NSMutableArray alloc] init];
        
        for (PFObject *set in activeSets) {
            [setVersions setObject:set[@"setVersion"] forKey:set[@"setNumber"]];
        }

        // Find the set data from the local datastore with that set number.
        PFQuery *localSetQuery = [PFQuery queryWithClassName:@"DiceMastersSet"];
        [localSetQuery fromLocalDatastore];
        NSArray *localSets = [localSetQuery findObjects];
        
        for (NSNumber *setNumber in [setVersions allKeys]) {
            
            // Compare the set numbers and ensure that they are up-to-date.
            bool updateToDateSet = false;
            for (PFObject *localSet in localSets) {
                
                if ([localSet[@"setNumber"] longValue] == [setNumber longValue] &&
                    [[setVersions objectForKey:setNumber] longValue] == [localSet[@"setVersion"] longValue]) {
                    updateToDateSet = true;
                }
            }
            
            // If they match, keep them, if not, add them to an update list.
            if (updateToDateSet) {
                NSLog(@"Set %ld is up to date", [setNumber longValue]);
            } else {
                NSLog(@"Set %ld is out of date", [setNumber longValue]);
                [setsToDownload addObject:setNumber];
            }
        }
        
        // Download all of the card data from the missing sets.
        NSLog(@"Downloading Sets: %@", setsToDownload);
        
        if (setsToDownload.count != 0) {
            
            PFQuery *newQuery = [PFQuery queryWithClassName:@"DiceMastersSet"];
            [newQuery whereKey:@"setNumber" containedIn:setsToDownload];
            [newQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                
                NSLog(@"Downloaded %lu Sets", objects.count);
                for (PFObject *saveObject in objects) {
                    [saveObject pin];
                    
                    // Download all of the cards required
                    PFQuery *cardQuery = [PFQuery queryWithClassName:@"Card"];
                    [cardQuery whereKey:@"setNumber" equalTo:saveObject[@"setNumber"]];
                    cardQuery.limit = 200;
                    [cardQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
                       
                        NSLog(@"Cards Downloaded: %lu", objects.count);
                        
                        // Pin them in the background to not block the UI Thread.
                        [self setValue:[NSNumber numberWithUnsignedLong:_cardsToLoad + 1] forKey:@"cardsToLoad"];
                        [PFObject pinAllInBackground:objects block:^(BOOL succeeded, NSError *error) {
                            [self setValue:[NSNumber numberWithUnsignedLong:_cardsLoaded + 1] forKey:@"cardsLoaded"];
                        }];
                    }];
                }
            }];
        } else {
            
            [self performSegueWithIdentifier:@"mainMenuSegue" sender:self];
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    
    // Keep the progress bar complete % in sync with the card set loading.
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        
        _progressText.hidden = NO;
        _progressText.text = [NSString stringWithFormat:@"Downloading Sets (%lu / %lu)", _cardsLoaded, _cardsToLoad];
        
        if (_cardsToLoad != 0) {
            _progressView.hidden = NO;
            _progressView.progress = (float)_cardsLoaded / (float)_cardsToLoad;
        }
    }];

    if (_cardsToLoad == _cardsLoaded) {
        [self performSegueWithIdentifier:@"mainMenuSegue" sender:self];
    }
    
}

@end
